import { Component, OnInit } from '@angular/core';
import { Blog } from './blog/blog.interface';
import { BlogsService } from './blog/blogs.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(private blogsService: BlogsService) { }

  blogs: Blog[]

  ngOnInit() {
    this.blogsService.getBlogs().subscribe(res => {
      this.blogs = res
    }, err => {
      console.error(err)
    })
  }

  blogDeleted(id: string) {
    this.blogs = this.blogs.filter(b => b._id !== id)
  }

}
