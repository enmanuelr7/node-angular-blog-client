import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatDate'
})
export class FormatDatePipe implements PipeTransform {

  constructor(
    private datePipe: DatePipe
  ) { }

  transform(value: string): string {
    const now = new Date()
    const date = new Date(value)
    let isThisMonthAndYear = !(now.getMonth() - date.getMonth()) && !(now.getFullYear() - date.getFullYear())

    if (isThisMonthAndYear) {
      switch (now.getDate() - date.getDate()) {
        case 0:
          return this.datePipe.transform(value, 'hh:mm a').toLowerCase()
        case 1:
          return 'Yesterday'
        default:
          return this.datePipe.transform(value, 'MMM d')
      }
    } else {
      return this.datePipe.transform(value, 'MMM d')
    }

  }

}
