import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Blog } from './blog.interface';
import { BlogsService } from './blogs.service';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {
  @Input() blog: Blog
  @Output() blogDeleted = new EventEmitter<string>();
  isDelete = false;

  constructor(private readonly blogsService: BlogsService) { }

  ngOnInit(): void {
  }

  deleteBlog(e: MouseEvent, blogId: string) {
    this.isDelete = true;
    this.blogsService.deleteBlog(blogId).subscribe(() => {
      this.blogDeleted.emit(blogId)
    }, err => {
      console.error(err)
      this.isDelete = false
    })
    e.preventDefault()
  }

}
