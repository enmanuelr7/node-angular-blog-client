import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Blog } from './blog.interface';

@Injectable({
  providedIn: 'root'
})
export class BlogsService {

  url = environment.url;

  constructor(private http: HttpClient) { }

  getBlogs(): Observable<Blog[]> {
    return this.http.get<Blog[]>(this.url)
  }

  deleteBlog(id: string) {
    return this.http.delete(`${this.url}/${id}`)
  }

}
